## [Overview](#overview)

This list does not include specific [employees or ex-employee](https://www.labourblawg.com/employment-law/recent-lawsuits-filed-by-employees-against-microsoft/), the reason is that I want to show the whole picture and individual stuff like this is too much to list here. I make one exception because the case went up to supreme court. It should be noted that the harassment section does include and mention specific employees or ex-employees due to the nature of the accusation and because there might be pending lawsuits or framing involved from Microsoft's end.

Please keep in mind that actions of some individuals normally do not reflect the whole company unless they represent them directly like CEO etc.

Microsoft itself [tend to kill all of their own products](https://killedbymicrosoft.info/), see also [here](https://www.versionmuseum.com/history-of/discontinued-microsoft-products), first they buy any competitor and then after some years when they see there is nothing more out from it to gain, they close their project and repeat the process.

Keep in mind that copyright laws vary wildly across countries and jurisdictions. I only list the stuff that is proven and one example - DOS - that cannot entirely confirmed nor denied, this case in history is a special and the reason why it got listed.

-----------------------

## [Ads in Microsoft Products and Operating system](#ads-in-microsoft-products-and-operating-system)
Microsoft took the ads idea from Apple, later Google who integrated ads into Apple Music, Apple Fitness, iCloud etc. and GMail.

- [Windows 11 File Explorer Might Have "Ads" (2022)](https://www.onmsft.com/news/windows-11-file-explorer-ads), see [here](https://twitter.com/flobo09/status/1502645866204704773s). This is another attempt and the story [goes back into 2017](https://www.extremetech.com/computing/245553-microsoft-now-puts-ads-windows-file-explorer).
- [Windows 10 ads in lock screen (2015)](https://www.theverge.com/2015/4/29/8514345/windows-spotlight-lock-screen-with-ads)
- [Windows 10 ads in start menu (2016)](https://www.theverge.com/2016/5/16/11682164/microsoft-windows-10-promoted-apps-start-menu-anniversary-update)
- [Windows 10 ad pop-ups (2017)](https://www.theverge.com/2017/3/17/14956540/microsoft-windows-10-ads-taskbar-file-explorer)
- [Ads in Lockscreen in latest Insider Build shortly after MS got busted putting ads in Explorer.exe (2022)](https://twitter.com/CKsTechNews/status/1508341789513691136)



## [Antitrust History](#antitrust-history)

I cannot list all 1k+ antitrust files, some of them never made it public because Microsoft handled them internally which means they paid the victim or Corp. before it went ever public.


> Microsoft is committed to the highest standards of business ethics, including fair competition and compliance with antitrust laws.

Above statement is taken from Microsoft's own [Antitrust Homepage](https://www.microsoft.com/en-us/legal/antitrust), which they created AFTER they lost several antitrust cases.


- [August 15, 2022 - Microsoft sharply curtails class action rights (2022)](https://www.microsoft.com/en-us/servicesagreement/upcoming-updates.aspx)
- [An oral history of the Microsoft antitrust lawsuit (2018)](https://www.theringer.com/tech/2018/5/18/17362452/microsoft-antitrust-lawsuit-netscape-internet-explorer-20-years)
- [Apple-Microsoft Lawsuit Fizzles To A Close (1993)](https://community.seattletimes.nwsource.com/archive/?date=19930602&slug=1704430)
- [China ditches Windows in the next three years due to antitrust (2022)](https://www.techradar.com/news/china-to-ditch-all-windows-pcs-by-2022-could-this-be-linuxs-time-to-shine)
- [Design company Bing sues Microsoft over trademark (2009)](http://www.macworld.com/article/145180/2009/12/bing_lawsuit.html?lsrc=rss_main)
- [European Commission fines Microsoft $613M in antitrust case (2004)](https://www.computerworld.com/article/2564087/european-commission-fines-microsoft--613m-in-antitrust-case.html)
- [Gates testifies in $1B lawsuit against Microsoft (2011)](https://phys.org/news/2011-11-gates-testifies-1b-lawsuit-microsoft.html), [$1B lawsuit against Microsoft Dismissed](https://finance.yahoo.com/news/jurors-deadlock-1b-lawsuit-against-microsoft-022648719.html)
- [Getty Images sues Microsoft over new online photo tool (2014)](http://www.reuters.com/article/2014/09/04/getty-images-microsoft-lawsuit-idUSL1N0R52DI20140904)
- [IP Antitrust Issues in The Microsoft Case (2000)](https://scholarlycommons.law.northwestern.edu/cgi/viewcontent.cgi?article=1105&context=njtip&httpsredir=1&referer=)
- [Judge orders Microsoft to stop selling Word (2009)](https://www.usatoday.com/tech/news/2009-08-12-microsoft-lawsuit_N.htm)
- [Microsoft Corp. v. Commission](https://en.wikipedia.org/wiki/Microsoft_Corp._v._Commission) + [United States v. Microsoft Corp.](https://en.wikipedia.org/wiki/United_States_v._Microsoft_Corp.)
- [Microsoft Federal Litigation Filings (History - Upcoming lawsuits)](https://companyprofiles.justia.com/company/microsoft/dockets/case)
- [Microsoft Gender Discrimination Class Action Lawsuit (2018)](https://www.business-humanrights.org/de/neuste-meldungen/judge-blocks-effort-to-expand-microsoft-gender-bias-case-to-thousands-of-women/)
- [Microsoft Gender Discrimination and Unequal Pay Lawsuit Investigation (2019)](https://www.classlawgroup.com/microsoft-gender-discrimination-and-unequal-pay-lawsuit/)
- [Microsoft Software Class Action Settlement (2021)](https://ca.topclassactions.com/lawsuit-settlements/open-lawsuit-settlements/microsoft-software-class-action-settlement/)
- [Microsoft and TiVo call truce on patent lawsuits (2012)](https://arstechnica.com/tech-policy/news/2012/03/microsoft-and-tivo-call-truce-on-patent-lawsuits.ars)
- [Microsoft faces lawsuit over Silverlight (2008)](https://www.cnet.com/culture/microsoft-faces-lawsuit-over-silverlight/)
- [Microsoft hit by patent lawsuit over Skype (2013)](https://news.cnet.com/8301-13578_3-57582349-38/microsoft-hit-by-patent-lawsuit-over-skype/)
- [Microsoft joins Epic Games lawsuit to stop Apple's antitrust behavior (2022)](https://appleinsider.com/articles/22/02/01/microsoft-says-that-if-apple-isnt-stopped-now-its-antitrust-behavior-will-just-get-worse) and [Apple suggests Microsoft could be behind the Epic Games lawsuit (2021)](https://9to5mac.com/2021/05/21/epic-games-lawsuit-microsoft/)
- [Microsoft joins group seeking legal immunity from climate change lawsuits (2019)](https://www.theguardian.com/technology/2019/may/01/microsoft-joins-group-seeking-to-avoid-climate-change-lawsuit)
- [Microsoft litigation](https://en.wikipedia.org/wiki/Microsoft_litigation)
- [Microsoft, Amazon and Google violated Illinois face recognition privacy law (2020)](https://techcrunch.com/2020/07/15/facial-recognition-lawsuit-vance-janecyk-bipa/)
- [Supreme Court rules against Microsoft in retaliation lawsuit by ex-employee (2018)](https://www.seattletimes.com/seattle-news/washington-supreme-court-rules-against-microsoft-in-alleged-retaliation-lawsuit/)
- [The IRS Decided to Get Tough Against Microsoft. Microsoft Got Tougher (2020)](https://www.propublica.org/article/the-irs-decided-to-get-tough-against-microsoft-microsoft-got-tougher)
- [US government opposes Microsoft in Supreme Court patent lawsuit (2011)](https://blog.seattlepi.com/microsoft/2011/03/21/us-government-opposes-microsoft-in-supreme-court-patent-lawsuit/)
- [Microsoft Faces Antitrust Complaint in Europe About Its Cloud Services (2022)](https://www.wsj.com/articles/microsoft-faces-antitrust-complaint-in-europe-about-its-cloud-services-11647463334)
- [French cloud provider OVH filed an anti-trust complaint against Microsoft due to the company’s obvious unfair advantage and uneven playing field (2021)](https://help.nextcloud.com/t/eu-cloud-providers-subject-to-lock-in-with-microsoft-due-to-new-strategy/140534)



## [Against user will and without consent](#against-user-will-and-without-consent)
- [Microsoft banned a user from playing Minecraft until he provides personal info (2022)](https://twitter.com/vkoskiv/status/1549109139296632833)
- [Microsoft rolls back default macro blocks in Office without telling anyone (2022)](https://www.theregister.com/2022/07/08/office_macro_block_rollback/)
- [MS lets partners change AD privileges on customer systems – without permission (2022)](https://www.theregister.com/2022/07/01/gdap_permissionless_change_window/)
- [Microsoft stops selling emotion-reading tech, limits face recognition (2022)](https://www.reuters.com/technology/microsoft-stops-selling-emotion-reading-tech-limits-face-recognition-2022-06-21/)
- [Win 10 hardware must support Secure Boot and won't have to let you turn it off (2022)](https://arstechnica.com/information-technology/2015/03/windows-10-to-make-the-secure-boot-alt-os-lock-out-a-reality/)
- [DuckDuckGo paid by Microsoft to not block their trackers (2022)](https://twitter.com/shivan_kaul/status/1528879590772338689)
- [Microsoft Update Quietly Installs Firefox Extension (2009)](http://voices.washingtonpost.com/securityfix/2009/05/microsoft_update_quietly_insta.html)
- [Windows Phone 7 also spies on you, according to lawsuit (2011)](https://www.pcworld.com/article/482449/microsoft_sued_accused_of_collecting_windows_phone_7_location_data.html)
- [Windows 11 Officially Shuts Down Firefox’s Default Browser Workaround (2021)](https://www.howtogeek.com/774542/windows-11-officially-shuts-down-firefoxs-default-browser-workaround/)
- [What happens if you try to download and install Firefox on Windows (2022)](https://twitter.com/plexus/status/1510568329303445507)
- [New Windows 11 preview Build 22616 makes Microsoft accounts mandatory for (almost) all (2022)](https://www.pcworld.com/article/699100/microsoft-extinguishes-windows-11-pro-account-loophole-in-latest-build.html)
- [Microsoft does not let you force log out of all sessions - unresolved (2013)](https://answers.microsoft.com/en-us/outlook_com/forum/all/how-can-i-force-a-logout-on-all-devices/6b1439bc-bfff-45c2-97ce-30039ea6261d)



## [Anti-Consumer and Anti-competitive](#anti--consumer-and-anti-competitive)
- [Bing is censoring search results for Alex Berenson’s Substack (2022)](https://reclaimthenet.org/bing-censoring-alex-berenson-unreported-truths-substack/)
- [Microsoft blocks Tutanota users from own service (2022)](https://tutanota.com/blog/posts/microsoft-blocks-tutanota-users-from-own-service/)
- [Xbox Cloud throttles performance if user agent is Linux (2022)](https://old.reddit.com/r/xcloud/comments/vrfmuz/quality_on_linux/)
- [Bing Wallpaper tool suggest other settings for MS Edge (2022)](https://grumpy.website/post/0XD7TGno1)
- [Microsoft: An Unprincipled, Rapacious Company (1995)](https://www.spectacle.org/495/ms.html)
- [A History of Anticompetitive Behavior and Consumer Harm (2002)](https://www.justice.gov/atr/competitive-processes-anticompetitive-practices-and-consumer-harm-software-industry-analysis)
- [After a lawsuit with Microsoft, Katie Moussouris is fighting for fair pay (2021)](https://www.theverge.com/22331972/pay-equity-now-pledge-katie-moussouris-microsoft-lawsuit)
- [Competition Killer: European Commission Documents 20 Years of Microsoft Anticompetitive Behavior (2009)](https://www.linux-magazine.com/Online/News/Competition-Killer-European-Commission-Documents-20-Years-of-Microsoft-Anticompetitive-Behavior)
- [E.U.'s Anti-Microsoft Vendetta Finally Provokes Consumer Backlash (2006)](https://www.cfif.org/htdocs/legislative_issues/federal_issues/hot_issues_in_congress/technology/Anti-Microsoft-Vendetta-Finally-Provokes-Consumer-Backlash.htm)
- [How LinkedIn exfiltrates extension data from the browser](https://prophitt.me/articles/nefarious-linkedin), since 2016 LinkedIn is [owned by Microsoft](https://news.microsoft.com/2016/06/13/microsoft-to-acquire-linkedin/).
- [How Microsoft Appointed Itself Sherrif of the Internet (2014)](https://www.slate.com/articles/technology/technology/2014/10/no_ip_what_microsoft_lawsuits_have_done_for_security_and_software_companies.html)
- [Microsoft Takes Fight With i4i Over XML To The Supreme Court (2010)](https://techcrunch.com/2010/08/27/lawsuit-day-continues/)
- [Microsoft behind Google's anti-trust lawsuit with Tiny TradeComet (2011)](https://blogs.wsj.com/law/2011/07/26/one-small-antitrust-victory-for-google/)
- [Microsoft blocks EdgeDeflector to force Windows 11 users into Edge (2021)](https://www.theverge.com/2021/11/15/22782802/microsoft-block-edgedeflector-windows-11)
- [Microsoft changes the way windows 10 updates after lawsuit (2016)](https://www.bbc.com/news/technology-36657890)
- [Microsoft plans to use license agreements to prevent class action lawsuits (2012)](https://www.cultofmac.com/170183/you-cant-legally-join-a-class-action-lawsuit-against-microsoft-but-you-can-against-apple-for-now/)
- [Monopoly and Anticompetitive Practices of Microsoft’s, Essay Example (2008)](https://essays.io/monopoly-and-anticompetitive-practices-of-microsofts-essay-example/)
- [Original Halo composers sue Microsoft over unpaid royalties dating back 20 years (2022)](https://www.eurogamer.net/articles/2022-02-10-original-halo-music-composers-threaten-to-try-to-block-tv-show-amid-lawsuit-with-microsoft-over-unpaid-royalties)
- [Windows 11 Pro to require internet connection and Microsoft Account (2022)](https://blogs.windows.com/windows-insider/2022/02/16/announcing-windows-11-insider-preview-build-22557/) starting with Insider Build 22557, previously only Home Builds were affected.
- [Windows 8 EULA Prohibits Class Action Lawsuits Against Microsoft (2012)](https://www.forbes.com/sites/adriankingsleyhughes/2012/05/30/windows-8-eula-prohibits-class-action-lawsuits-against-microsoft/)
- [Outlook broken on Firefox for 6 months – MS recommend switch to Edge (2022)](https://answers.microsoft.com/en-us/outlook_com/forum/all/working-with-outlook-web-interface-on-firefox/4212cac3-4be3-4295-b325-369ea3d46f2a?page=6)
- [Microsoft v. MikeRoweSoft (2003)](https://en.wikipedia.org/wiki/Microsoft_v._MikeRoweSoft)
- [Google: Bing is cheating, copying our search results (2011)](https://searchengineland.com/google-bing-is-cheating-copying-our-search-results-62914)



## [GitHub under Microsoft Management](#github-under-microsoft-management)
- [GitHub removes promise not to track you (2022)](https://github.com/github/site-policy/pull/582)
- [GitHub suspends Tornado Cash developer account (2022)](https://twitter.com/semenov_roman_/status/1556717890308653059) + [GitHub accounts of people who contributed to Tornado Cash repos just got deleted (2022)](https://twitter.com/bantg/status/1556721709931175937), apparently GitHub banned him without any evidence just because [others abused his network for money laundering](https://www.abc.net.au/news/2022-08-09/us-cracks-down-on-crypto-mixer-site-tornado/101315300)
- GitHub has long sought to discredit copyleft generally. Their various CEOs have often spoken loudly and negatively about copyleft, including their founder (and former CEO) devoting [his entire OSCON keynote on attacking copyleft and the GPL](https://www.youtube.com/watch?v=-bAAlPXB2-c). There are also [examples of GitHub employees filing bug tickets in copylefted projects to cajole them to change to non-copyleft licenses](https://github.com/BenKallos/legislation/issues/2#issue-46911010).
- While GitHub pretends to be pro-FOSS ([like SourceForge before them](https://lwn.net/Articles/17822/)), their entire hosting site is, itself, proprietary and/or trade-secret software. GitHub does not even offer any self-hosting FOSS option.
- [In 2020, the community discovered that GitHub has a for-profit software services contract with the USA Immigration and Customs Enforcement (ICE) (2020)](https://www.theatlantic.com/technology/archive/2020/01/ice-contract-github-sparks-developer-protests/604339/)
- [GitHub Copilot just sells code other people wrote (2022)](https://twitter.com/ReinH/status/1539626662274269185)
- [GitHub waited 3 months to notify about potential compromise (2022)](https://news.ycombinator.com/item?id=31769520)
- [Microsoft: We loves open source developers ... not? (2022)](https://gist.github.com/slimsag/c01bb6508e3dfa744bf3bdafa0cfe07f)
- [GitHub Kills Atom Text Editor (2022)](https://www.youtube.com/watch?v=dGMl8c0Cmzs), see [GitHub Announce (2022)](https://github.blog/2022-06-08-sunsetting-atom/), [Microsoft discloses plans to turn parts of VS Code C# extension to closed source (2022)](https://github.com/omnisharp/omnisharp-vscode/issues/5276)
- [Flagged/shadow banned on GitHub for praising s/w? (2020)](https://news.ycombinator.com/item?id=21052800)
- [GitHub account got suspended without any notice (2020)](https://news.ycombinator.com/item?id=23832437)
- [GitHub blocks repositories (in Russia) requested by Russian government no matter if those requests are bogus or not (2022)](https://github.com/github/gov-takedowns/blob/master/Russia/2022/2022-02-22-Roskomnadzor/2022-02-22-Roskomnadzor.md)
- [GitHub can't be trusted: suspending Russian accounts deleted history and PRs (2022)](https://www.jessesquires.com/blog/2022/04/19/github-suspending-russian-accounts/)
- [GitHub flagged account with a repo containing kernel mode attack/driver (2020)](https://news.ycombinator.com/item?id=21907621)
- [Hello, GitHub - GitHub flags accounts criticizing Microsoft acquisition (2018)](https://natfriedman.github.io/hello/)
- [Since Microsoft took over more accounts got flagged than ever (2018)](https://github.community/t/my-account-is-flagged/282) which is a nice way to get rid of competition as well as people who dare to speak against Microsoft.
- [GitHub deleted anti-censorship activist repositories (2018)](https://natfriedman.github.io/hello/)
- [How and Why GitHub Censors Repositories (2019)](https://github.com/saniv/text/blob/master/github-censorship.md)



## [Harassment](#harassment)
- [Alex Kipman has left Microsoft following sexual misconduct allegations (2022)](https://www.eurogamer.net/microsofts-alex-kipman-has-left-the-company-following-sexual-misconduct-allegations)
- [“Women at Microsoft are sexualized by their male managers,” lawsuit alleges (2018)](https://arstechnica.com/tech-policy/2018/03/sexual-harassment-at-microsoft-often-goes-unpunished-lawsuit-alleges/)
- [Moderators who had to view child abuse content sue Microsoft, claiming PTSD (2017)](https://www.theguardian.com/technology/2017/jan/11/microsoft-employees-child-abuse-lawsuit-ptsd?CMP=twt_a-technology_b-gdntech)



## [Lie about security chip](#lie-about-security-chip)
- [Meet the Microsoft Pluton processor – The security chip designed for the future of Windows PCs (2020)](https://www.microsoft.com/security/blog/2020/11/17/meet-the-microsoft-pluton-processor-the-security-chip-designed-for-the-future-of-windows-pcs/), years later [Intel isn't using Microsoft's Pluton security in Alder Lake (2022)](https://www.theregister.com/2022/03/02/microsoft_pluton_chip/).



## [Market Manipulation](#market-manipulation)
There is an entire forum for Microsoft regarding manipulations, see [here](https://www.seattlepi.com/business/article/Microsoft-s-orange-badge-culture-gets-forum-1191262.php).


- [Microsoft’s proposed policy to ban commercial open source apps on MS Store (2022)](https://techcrunch.com/2022/07/15/dissecting-microsofts-delayed-policy-to-ban-commercial-open-source-apps/) only [after huge pressure Microsoft reverted the change](https://sfconservancy.org/news/2022/jul/18/foss-for-sale-in-ms-app-store/)
- [Latest Windows 11 update changed Chrome config, switched default browser to Edge (2022)](https://old.reddit.com/r/sysadmin/comments/sbgmab/last_windows_11_update_changed_default_browser_to/)
- [EU VS. Microsoft (2007)](https://www.cnet.com/culture/eu-vs-microsoft-the-morning-after/)



## [Stolen from Apple](#stolen-from-apple)
- Calculator function of Spotlight.
- Continuity Continuum.
- Free upgrades concept in eg. Windows 7, 8, 10, 11
- Microsoft Store, which is copied from Linux and or macOS.
- Right-click using two fingers.
- Screen Sharing, also known as Remote Desktop Connection, or shall we say NXHost and it was in NeXTStep long before Microsoft or Apple had it.
- Siri becomes basically Cortana and is integrated into the OS, later Microsoft decided to make it an App.
- Spotlight renamed to Search box in the bottom left corner and integrated into Cortana.
- Track pad that performs multiple functions.
- [Windows 2.0 borrowed several elements from the Mac user interface (1985)](https://www.cultofmac.com/470399/today-in-apple-history-microsoft-sued-ripping-off-mac-os/).



## [Stolen Concepts and Ideas](#stolen-concepts-and-ideas)
- [Microsoft is launching a Facebook rip-off inside Teams (2022)](https://www.theverge.com/2022/7/19/23268187/microsoft-viva-engage-facebook-work-yammer).
- [Microsoft insults dev, then takes credit for their idea (2022)](https://twitter.com/cmuratori/status/1522468481135902725).
- DOS, [which might never revealed if this story is real or not (2012)](https://www.wired.com/2012/08/ms-dos-examined-for-thef/).
- KDEs emoji selector
- [Google: Bing Is Copying Our Search Results (2011)](https://searchengineland.com/google-bing-is-cheating-copying-our-search-results-62914)
- [KDE Connect (2018)](https://old.reddit.com/r/linuxmasterrace/comments/8hr8kl/microsoft_copies_more_things_from_linux_this_time/)
- [KDE Plasma (2021)](https://old.reddit.com/r/Windows11/comments/o9xxu9/thanks_microsoft_for_stealing_a_linux_distro/)
- [KDE Weather widget (2021)](https://www.windowslatest.com/2021/12/12/closer-look-at-new-weather-widget-for-windows-11-taskbar/)
- [Microsoft Ripping Off KDE 4.2 For Windows 7 (2009)](https://ubuntuforums.org/archive/index.php/t-1103549.html)
- [Microsoft is bringing stickers to Windows 11’s desktop (2022)](https://www.windowslatest.com/2022/02/07/microsoft-is-bringing-stickers-to-windows-11s-desktop/)
- [Stolen ad from the KDE community (2021)](https://old.reddit.com/r/linux/comments/r6popu/its_been_155_days_since_microsoft_stole/)
- [Stolen from Brave, Transparent ads (2022)](https://techcommunity.microsoft.com/t5/articles/introducing-transparent-ads-in-microsoft-edge-preview/m-p/3035970#M6283)
- [Stolen XBOX One concept ad (2018)](https://old.reddit.com/r/PUBATTLEGROUNDS/comments/7md8oz/remember_my_xbox_one_ad_concept_microsoft_just/)
- [Web widgets (2014)](https://www.onmsft.com/news/microsoft-stole-my-idea-and-now-making-billions-out-it-local-resident-starts-online-petition)
- [Winget packaging ripoff (2020)](https://www.thurrott.com/windows/windows-10/235783/appget-creator-says-microsoft-stole-his-product). Microsoft asked the original inventor some questions, sherlocked his software, even [didn't thank him later](https://www.theverge.com/2020/5/28/21272964/microsoft-winget-windows-package-manager-appget-copied), [only after huge media pressure](https://www.theverge.com/2020/6/2/21277863/microsoft-winget-windows-package-manager-appget-response-credit-comment).

For more criticism check out the [Wikipedia entry](https://en.wikipedia.org/wiki/Criticism_of_Microsoft).


## [Taking over Open Source Licenses](#taking-over-open-source-licenses)

After Microsoft got busted they said it was a Bot who automatically adjust licenses for forks, for employees. Which is weird, they let it stand for weeks and months.

- [GitHub Arctic Code Vault Project (2020)](https://archiveprogram.github.com/arctic-vault/) stole older members project source code, made is closed, which then violates the licenses because you must show the code at all time. God knows what they really do with the code, maybe they train their AI to make money out of it, we will never know since no one gets access to it nor is there a website which shows the code. Later in 2022 they continue to do it again, [this time with Music (2022)](https://singularityhub.com/2022/06/12/microsoft-to-archive-music-on-futuristic-slivers-of-glass-that-will-live-10000-years/)
- [GitHub Copilot is trained on public GitHub repositories of any license](https://en.m.wikipedia.org/wiki/GitHub_Copilot#Technology) without asking users.
- [Microsoft forks MIT licensed repo, and changes the copyright to them (2021)](https://github.com/microsoft/grpc_bench/commit/04c7143a39a0bb243369e31f3b3b797449468fdb), which seems [not be the first time](https://www.reddit.com/r/opensource/comments/roa9xz/microsoft_fork_changing_the_license/), Microsoft manipulating licenses by replacing original licenses with their own.



## [Telemetry in Products](#telemetry-in-products)

The benefit of telemetry in software is questionable. Microsoft and other big players can collect and sell statistics or use given data to maintain full control over the market, because the data might contain sensitive data that can be used to in your own projects and products which gives you an advantage over the competition.

- [Azure Event Hubs – Cloud-scale telemetry ingestion (2018)](https://azure.microsoft.com/en-us/services/event-hubs/)
- [Microsoft .Net Core telemetry is not opt-in (2018)](https://github.com/dotnet/cli/issues/3093#issuecomment-392663561)
- [Microsoft Office Telemetry (2020)](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Studien/Office_Telemetrie/Office_Telemetrie.pdf?__blob=publicationFile&v=5)
- [Microsoft adds Telemetry to Windows 7 and 8.1 with an update (2014)](https://support.microsoft.com/en-us/kb/3068708)
- [Microsoft collects telemetry in onnxruntime Python module (2019)](https://github.com/microsoft/onnxruntime/releases/tag/v1.0.0)
- [Microsoft repo secretly installed on all Raspberry Pi’s Linux OS (2021)](https://www.cyberciti.biz/linux-news/heads-up-microsoft-repo-secretly-installed-on-all-raspberry-pis-linux-os/)
- [Microsoft shares Windows 10 telemetry data with third parties (2016)](http://betanews.com/2016/11/24/microsoft-shares-windows-10-telemetry-data-with-third-parties/)
- [Microsoft's Software is Malware (2011)](https://www.gnu.org/proprietary/malware-microsoft.en.html)
- [Microsoft's Windows 10 breaches privacy law, says Dutch DPA (2017)](https://techcrunch.com/2017/10/13/microsofts-windows-10-breaches-privacy-law-says-dutch-dpa/)
- [Microsoft's new Edge shares “persistent” device identifiers to back end servers (2020)](https://threatpost.com/microsoft-edge-privacy-busting-telemetry/153733), see [Microsoft Edge has more privacy-invading telemetry than other browsers](https://betanews.com/2020/03/09/microsoft-edge-privacy-telemetry/)
- [Microsoft/.Net Foundation added telemetry to the dotnet command line last year (2017)](https://opinionatedgeek.com/Blog/2017/3/26/your-tools-shouldn-t-spy-on-you), see [What We’ve Learned from .NET Core SDK Telemetry](https://blogs.msdn.microsoft.com/dotnet/2017/07/21/what-weve-learned-from-net-core-sdk-telemetry/)
- [Reviewing Microsoft's Automatic Insertion of Telemetry into C++ Binaries (2016)](https://www.infoq.com/news/2016/06/visual-cpp-telemetry)
- [VSCode still sends search keystrokes to Microsoft with telemetry disabled (2018)](https://mstdn.io/@taoeffect/99968111410687610)
- [Windows 10: HOSTS file blocking telemetry is now flagged as a risk (2020)](https://www.bleepingcomputer.com/news/microsoft/windows-10-hosts-file-blocking-telemetry-is-now-flagged-as-a-risk/)
- [Windows Server and System Center 2016 Telemetry Whitepaper (2016)](https://www.content.shi.com/shicom/contentattachmentimages/sharedresources/pdfs/winsvr_systemctr_2016_telemetry_whitepaper.pdf)
- [You still can’t turn off Windows 10’s built-in spyware (2017)](https://www.computerworld.com/article/3159424/microsoft-windows/you-still-can-t-turn-off-windows-10-s-built-in-spyware.html)

